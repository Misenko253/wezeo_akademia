<?php namespace Miso\Userapi\Http\Controllers;
use http\Env\Response;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller;
use Auth;
use Miso\User\Models\User as UserModel;
use \Tymon\JWTAuth\Facades\JWTAuth;



class UserapiController extends Controller
{

    public function login(): JsonResponse {
        if(!$token = Auth()->attempt(input())) {
            return Response()->json(['error' => 'Invalid Credentials'], 401);
        }
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'user' => auth()->user(),
        ]);
    }

    public function register(): JsonResponse
    {
        $user = Auth::register([
            'name' => post('name'),
            'email' => post('email'),
            'password' => post('password'),
            'password_confirmation' => post('password_confirmation'),
        ]);
        return response()->json($user);
    }

    public function userProfile() {
        return auth()->user();
    }

    public function updateUser() {
        $user = auth()->user();
        $data = input();
        if (!$user->checkHashValue('password', input('password_current'))) {
            return 'invalid password';
        }
        $user->fill($data);
        $user->save();
        return $user;
    }

    public function removeUser() {
        $user = auth()->user();
        $user->delete();
        return 'user was deleted';
    }

    public function forgotPasswrd() {
        $user = UserModel::findByEmail(post('email'));
        if (!$user) {
            return 'something gone wrong :/';
        }
        $code = implode('!', [$user->id, $user->getResetPasswordCode()]);
        $data = [
            'code'     => $code,
            'name'     => $user->name,
            'username' => $user->username,
        ];
        \Mail::send('rainlab.user::mail.restore', $data, function ($message) use ($user) {
            $message->to($user->email);
        });
        return "ok";
    }

    public function resetPasswrd() {
        $parts = explode('!', post('code'));
        if (count($parts) != 2) {
            return 'code is invalid!';
        }
        list($userId, $code) = $parts;
        if (!$user = Auth::findUserById($userId)) {
            return 'invalid user!';
        }
        if (!$user->attemptResetPassword($code, post('password'))) {
            return "error happened.";
        }
        return "Password successfully changed!";
    }

    public function refreshToken(): JsonResponse{
        $newToken['token'] = auth()->refresh();
        return response()->json($newToken);
    }

    public function invalidateToken(): JsonResponse{
        auth()->invalidate();
        $response['message'] = "Token was invalidated.";
        return  response()->json($response);
    }


    //nefunguje

    public function sendActivationCode() {

    }

    public function activateUser() {
        $user = auth()->user();
        if(!$user->attemptActivation(post('code')))return "fail";
    }
}
