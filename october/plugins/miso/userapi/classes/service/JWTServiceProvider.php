<?php namespace Miso\Userapi\Classes\Service;

use Illuminate\Auth\Middleware\Authenticate;
use Illuminate\Foundation\AliasLoader;
use October\Rain\Support\Facades\Config;
use October\Rain\Support\ServiceProvider;

class JWTServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Config::set('jwt', Config::get('miso.jwtauth::jwt'));

        $facade = AliasLoader::getInstance();
        $facade->alias('JWTAuth', '\Tymon\JWTAuth\Facades\JWTAuth');
        $facade->alias('JWTFactory', '\Tymon\JWTAuth\Facades\JWTFactory');

        $this->app['router']->aliasMiddleware('auth', Authenticate::class);
    }
}
