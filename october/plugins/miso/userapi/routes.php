<?php
Route::group([
    'prefix' => 'user_api',
    'namespace' => 'Miso\Userapi\Http\Controllers'
], function() {
    Route::post('/register', 'UserapiController@register');
    Route::post('/login', 'UserapiController@login');
    Route::post('forgot_password', 'UserapiController@forgotPasswrd');
    Route::post('reset_password', 'UserapiController@resetPasswrd');
});
Route::group([
    'prefix' => 'user_api/profile',
    'namespace' => 'Miso\Userapi\Http\Controllers',
    'middleware' => 'auth:api'
], function() {
    Route::get('refresh_token', 'UserapiController@refreshToken');
    Route::get('invalidate_token', 'UserapiController@invalidateToken');
    Route::patch('update_user', 'UserapiController@updateUser');
    Route::get('/', 'UserapiController@userProfile');
    Route::delete('delete', 'UserapiController@removeUser');
    Route::get('get_code', 'UserapiController@sendActivationCode');
    Route::post('activate', 'UserapiController@activateUser');
});

