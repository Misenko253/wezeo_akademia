<?php namespace Miso\Userapi;

use App;
use Backend\Facades\Backend;
use Illuminate\Auth\AuthManager;
use Illuminate\Auth\AuthServiceProvider;
use Illuminate\Auth\Middleware\Authenticate;
use Illuminate\Contracts\Auth\Factory as AuthFactory;
use Illuminate\Foundation\AliasLoader;
use October\Rain\Support\Facades\Config;
use Miso\Userapi\Classes\Service\JWTServiceProvider;
use System\Classes\PluginBase;
use Tymon\JWTAuth\Providers\LaravelServiceProvider;

/**
 * Userapi Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Userapi',
            'description' => 'No description provided yet...',
            'author'      => 'Miso',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {
        App::register(JWTServiceProvider::class);

        App::register(AuthServiceProvider::class);
        $this->app->alias('auth', AuthManager::class);
        $this->app->alias('auth', AuthFactory::class);

        App::register(LaravelServiceProvider::class);

    }

}
