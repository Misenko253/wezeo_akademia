<?php

use Miso\Arrival\Models\Arrival;
use Rainlab\User\Models\User;
use Illuminate\Support\Facades\Event;
use Miso\Arrival\Http\Resources\ArrivalResource;
use Miso\User\Http\Resources\UserResource;
use Firebase\JWT\Key;
use Firebase\JWT\JWT;




Route::get('/arrivals', function () {
    return ArrivalResource::collection(Arrival::get());

});

Route::post('/new_arrival', function() {
    $arrival = Arrival::create(post());
    return new ArrivalResource($arrival);
});

Route::post('/usr_arrivals', function() {
    $jwt = post('token');
    $key = 'example_key';
    $decoded = JWT::decode($jwt, new Key($key, 'HS256'));
    $user = User::find($decoded->id);
    $arrivals = $user->arrivals;
    return $arrivals;
});