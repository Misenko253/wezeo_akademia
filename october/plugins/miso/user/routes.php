<?php

use Miso\User\Models\Userlog;
use Firebase\JWT\JWT;

Route::post('/login', function() {
    $user = Auth::authenticate(post(), true);
    Auth::login($user, true);
    $userlog= new Userlog;
    $userlog->user_id = $user->id; 
    $userlog->save();
        

    $key = 'example_key';
    $payload = [
        'id' => "$user->id"
    ];
    $jwt = JWT::encode($payload, $key, 'HS256');
    return $jwt;
});
