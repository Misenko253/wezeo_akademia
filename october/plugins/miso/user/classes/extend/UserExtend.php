<?php namespace Miso\User\Classes\Extend;
use RainLab\User\Models\User;
use Miso\Arrival\Models\Arrival;
use Miso\Arrival\Models\Userlog;

class UserExtend
{
    public static function addArrivalRelationToUser() {
        User::extend(function ($user) {
            $user->hasMany['arrivals'] = Arrival::class;
        });
    }
    public static function addUserlogRelationToUser() {
        User::extend(function ($user) {
            $user->hasMany['userlogs'] = Userlog::class;
        });
    }
}