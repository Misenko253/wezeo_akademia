<?php namespace Miso\HookEvent;

use Backend;
use System\Classes\PluginBase;
use Miso\Arrival\Models\Arrival;
use October\Rain\Database\Model;
use October\Rain\Support\Facades\Flash;

/**
 * HookEvent Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'HookEvent',
            'description' => 'No description provided yet...',
            'author'      => 'Miso',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {
        Arrival::extend(function ($model) {
            $model->bindEvent('model.afterCreate', function () {
                $datetime = date("H:i:s");
                Flash::success($datetime);
            });
        });

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Miso\HookEvent\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'miso.hookevent.some_permission' => [
                'tab' => 'HookEvent',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'hookevent' => [
                'label'       => 'HookEvent',
                'url'         => Backend::url('miso/hookevent/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['miso.hookevent.*'],
                'order'       => 500,
            ],
        ];
    }
}
