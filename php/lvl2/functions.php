<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Prichod</title>
</head>
<body>
    <?php
        date_default_timezone_set("Europe/Bratislava");
        function VsetciStudenti() {
            $studenti = json_decode(file_get_contents("studenti.json"), true);
            return $studenti;
        }
        function NovyStudent($data) {
            $studenti = VsetciStudenti();
            // $data['id'] = rand(10, 999999999);
            array_push($studenti, $data);
            return $studenti;
        }
        function putJson($student) {
            file_put_contents("studenti.json", json_encode($student));
        }
        function prichod($name, $meskanie = "") {
            $data = array($name);
            $time = date("H:i");
            $prichod = "";
            if($time>="20" && $time <= "23:59") {
                die("nemozne");
            }
            if($time>"08" && $meskanie == "") {
                $time = $time . " meskanie";
            } 
            if($name !== "") {
                $name = "$name ";
            }
            if($meskanie == "") {
                $prichod = "$name$time \n";
            } elseif($meskanie !== "") {
                $prichod = "$name$time $meskanie\n";
            }
            $log = fopen("log.txt", "a") or die("Neda sa otvorit subor");
            fwrite($log, $prichod);
            fclose($log);
            echo $prichod;
            $id = count(VsetciStudenti());
            $data = array($name=>$id);
            putJson(NovyStudent($data));
        }
        prichod($_POST['name']);
        print_r(VsetciStudenti());
    ?>
</body>
</html>