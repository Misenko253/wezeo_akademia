<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Prichod</title>
</head>
<body>
    <?php
        date_default_timezone_set("Europe/Bratislava");
        class Student 
        {
            private static function studentsToJson($student) {
                file_put_contents("students.json", json_encode($student));
            }
            public static function studentList() {
                $students = json_decode(file_get_contents("students.json"), true);
                return $students;
            }
            public static function newStudent($data) {
                $students = Student::studentList();
                if ($students == null) {
                    $students = array();
                }
                array_push($students, $data);
                Student::StudentsToJson($students);
            }
        }
        class Arrival
        {
            public $time;
            public $id;
            private static function list() {
                $arrivals = json_decode(file_get_contents("arrivals.json"));
                return $arrivals;
            }
            public function newArrival($instArrival) {
                $arrivals = Arrival::list();
                if ($arrivals == null) {
                    $arrivals = array();
                }
                array_push($arrivals,$instArrival);
                file_put_contents("arrivals.json", json_encode($arrivals));
            }
            public static function writeLateArrival() {
                $arrivals = json_decode(file_get_contents("arrivals.json"));
                foreach($arrivals as $i => $timesObj) {
                    $time = $timesObj->time;
                    if ($timesObj->time > "08" && strpos($timesObj->time, 'meskanie') !== false) {
                    } elseif($timesObj->time > "08") {
                        $timesObj->time = $time . " meskaniew ";
                    }
                }
                file_put_contents("arrivals.json", json_encode($arrivals));
            }
        }
        function runArrival() {
            $namePost = $_POST['namePost'];
            $nameGet = $_GET['name'];
            if ($namePost != "") {
                arrival($namePost);
            }
            if($nameGet != "") {
                arrival($nameGet);
            }
        }
        function arrival($name, $lateArrival = "") {
            $data = array($name);
            $time = date("H:i");
            $arrival = "";
            if($time>="20" && $time <= "23:59") {
                die("nemozne");
            }
            if($time>"08" && $lateArrival == "") {
                $time = $time . " meskanie";
            } 
            if($name !== "") {
                $name = "$name ";
            }
            if($lateArrival == "") {
                $arrival = "$name$time \n";
            } elseif($lateArrival !== "") {
                $arrival = "$name$time $lateArrival\n";
            }
            $log = fopen("log.txt", "a") or die("Neda sa otvorit subor");
            fwrite($log, $arrival);
            fclose($log);
            echo $arrival;
            $id = count(Student::studentList());
            $data = array("name"=>$name, "id"=>$id);
            Student::newStudent($data);
            $instArrival = new Arrival();
            $instArrival->id = $id;
            $instArrival->time = $time;
            $instArrival->newArrival($instArrival);
        }
        runArrival();
        Arrival::writeLateArrival();
        echo "Vsetci studenti: ";
        print_r(Student::studentList());
    ?>
</body>
</html>